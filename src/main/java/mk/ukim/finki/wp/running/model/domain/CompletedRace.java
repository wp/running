package mk.ukim.finki.wp.running.model.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import mk.ukim.finki.wp.running.model.enumerations.Medal;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.type.SqlTypes;

import java.time.Duration;

@NoArgsConstructor
@Entity
@Data
public class CompletedRace {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    @JsonBackReference
    private Registration registeredRace;

    @Column(columnDefinition = "interval")
    @JdbcTypeCode(SqlTypes.INTERVAL_SECOND)
    private Duration runTime;

    private int placement;
    @Enumerated(EnumType.STRING)
    private Medal medal;
    private boolean participated;
    private boolean finished;
    @OneToOne
    private Voucher voucher;

    public CompletedRace(
            boolean participated,
            boolean finished,
            Duration runTime,
            int placement,
            Medal medal,
            Voucher voucher
    ) {
        this.runTime = runTime;
        this.placement = placement;
        this.finished = finished;
        this.voucher = voucher;
        this.medal = medal;
        this.participated = participated;
    }
}
