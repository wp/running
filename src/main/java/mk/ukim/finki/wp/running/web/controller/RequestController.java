package mk.ukim.finki.wp.running.web.controller;

import lombok.RequiredArgsConstructor;
import mk.ukim.finki.wp.running.model.exceptions.RequestByUserForSubEventAlreadyExistsException;
import mk.ukim.finki.wp.running.service.RequestService;
import mk.ukim.finki.wp.running.service.SubEventService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


import java.security.Principal;

@Controller
@RequestMapping("/request")
@RequiredArgsConstructor
public class RequestController {

    private final SubEventService subEventService;
    private final RequestService requestService;


    @GetMapping("/new/{subEventId}")
    public String showRequestForm(@PathVariable Long subEventId, Model model) {
        model.addAttribute("subEvent", subEventService.findById(subEventId));
        return "request/request-form";
    }


    @PostMapping("/new/{subEventId}")
    public String createRequest(@PathVariable Long subEventId,
                                @RequestParam String message,
                                Principal principal,
                                Model model,
                                RedirectAttributes redirectAttributes) {
        try {
            requestService.create(principal, subEventId, message);
            redirectAttributes.addFlashAttribute("successMessage", "Успешно аплициравте!");
            return "redirect:/subEvents/details/" + subEventId;
        } catch (RequestByUserForSubEventAlreadyExistsException e) {
            model.addAttribute("subEvent", subEventService.findById(subEventId));
            model.addAttribute("errorMessage", "Веќе имате аплицирано за оваа трка");
            return "request/request-form";
        }
    }

    @GetMapping("/delete/{id}")
    public String deleteRequest(@PathVariable Long id, Model model) {
        model.addAttribute("request", requestService.findById(id));
        return "request/confirm-delete";
    }

    @PostMapping("/delete/{id}")
    public String deleteRequest(@PathVariable Long id) {
        this.requestService.delete(id);
        return "redirect:/user-requests";
    }
}
