package mk.ukim.finki.wp.running.model.DTO;

import lombok.Data;

@Data
public class UpdateCompletedRaceDto {
    private boolean participated;
    private boolean finished;
    private Integer hr, min, sec;
    private Integer placement;
    private String medal;
    private boolean voucher;

    public UpdateCompletedRaceDto(
            Boolean participated,
            Boolean finished,
            Integer hr, Integer min, Integer sec,
            Integer placement,
            String medal,
            Boolean voucher
    ) {
        this.participated = participated != null && participated;
        this.finished = finished != null && finished;
        this.hr = hr;
        this.min = min;
        this.sec = sec;
        this.placement = placement;
        this.medal = medal;
        this.voucher = voucher != null && voucher;
    }
}
