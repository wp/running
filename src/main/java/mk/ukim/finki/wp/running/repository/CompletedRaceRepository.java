package mk.ukim.finki.wp.running.repository;

import mk.ukim.finki.wp.running.model.domain.CompletedRace;
import mk.ukim.finki.wp.running.model.enumerations.Medal;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CompletedRaceRepository extends JpaSpecificationRepository<CompletedRace, Long> {
    interface MedalCountProjection {
        Medal getMedal();
        long getCount();
    }

    @Query("SELECT cr.medal AS medal, COUNT(cr.medal) AS count " +
            "FROM CompletedRace cr " +
            "WHERE cr.registeredRace.user.id = :userId " +
            "GROUP BY cr.medal")
    List<MedalCountProjection> countMedalsByUserId(@Param("userId") String userId);
}

