package mk.ukim.finki.wp.running.config;

import jakarta.annotation.PostConstruct;
import mk.ukim.finki.wp.running.model.domain.Event;
import mk.ukim.finki.wp.running.model.domain.SubEvent;
import mk.ukim.finki.wp.running.model.domain.User;
import mk.ukim.finki.wp.running.model.domain.*;
import mk.ukim.finki.wp.running.model.enumerations.Medal;
import mk.ukim.finki.wp.running.model.enumerations.SubEventLength;
import mk.ukim.finki.wp.running.model.exceptions.ResultsNotFoundException;
import mk.ukim.finki.wp.running.repository.*;
import mk.ukim.finki.wp.running.service.ScoreAlgorithmService;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

@Component
public class DataInitializer {
    private final EventRepository eventRepository;
    private final UserRepository userRepository;
    private final SubEventRepository subEventRepository;
    private final RegistrationRepository registrationRepository;
    private final CompletedRaceRepository completedRaceRepository;
    private final ScoreAlgorithmService scoreAlgorithmService;

    public DataInitializer(EventRepository eventRepository, UserRepository userRepository, SubEventRepository subEventRepository, RegistrationRepository registrationRepository, CompletedRaceRepository completedRaceRepository, ScoreAlgorithmService scoreAlgorithmService) {
        this.eventRepository = eventRepository;
        this.userRepository = userRepository;
        this.subEventRepository = subEventRepository;
        this.registrationRepository = registrationRepository;
        this.completedRaceRepository = completedRaceRepository;
        this.scoreAlgorithmService = scoreAlgorithmService;
    }

    // Uncomment and run once to populate data
    @PostConstruct
    public void init() {
        if (this.eventRepository.count() == 0) {
            this.eventRepository.saveAndFlush(new Event("Skopje Marathon", "The biggest marathon in North Macedonia", LocalDateTime.of(2021, 5, 5, 10, 0), "Skopje"));
            this.eventRepository.saveAndFlush(new Event("Ohrid Marathon", "The most beautiful marathon in North Macedonia", LocalDateTime.of(2021, 6, 6, 10, 0), "Ohrid"));
            this.eventRepository.saveAndFlush(new Event("Bitola Marathon", "The oldest marathon in North Macedonia", LocalDateTime.of(2021, 7, 7, 10, 0), "Bitola"));

            this.eventRepository.saveAndFlush(new Event("Future Marathon", "The most beautiful marathon in North Macedonia", LocalDateTime.of(2025, 6, 6, 10, 0), "Ohrid"));
            this.eventRepository.saveAndFlush(new Event("SecFuture Marathon", "The oldest marathon in North Macedonia", LocalDateTime.of(2025, 7, 7, 10, 0), "Bitola"));
        }

        if (subEventRepository.count() == 0) {
            eventRepository.findAll().forEach(event -> {
                SubEvent subEvent = new SubEvent("5K Marathon", SubEventLength.FIVE, "image", "info", event.getTime());
                SubEvent subEvent2 = new SubEvent("10K", SubEventLength.TEN, "image", "info", event.getTime());
//                event.getSubEventsList().add(subEventRepository.saveAndFlush(subEvent));
//                event.getSubEventsList().add(subEventRepository.saveAndFlush(subEvent2));
                subEvent.setEvent(event);
                subEvent2.setEvent(event);

                eventRepository.saveAndFlush(event);
                subEventRepository.saveAndFlush(subEvent);
                subEventRepository.saveAndFlush(subEvent2);
            });
        }

        if (registrationRepository.count() == 0) {
            subEventRepository.findAll().forEach(subEvent -> {
                User user1 = userRepository.findAll().get(0);
                User user2 = userRepository.findAll().get(1);
                registrationRepository.saveAndFlush(new Registration(user1, subEvent, LocalDate.now()));
                registrationRepository.saveAndFlush(new Registration(user2, subEvent, LocalDate.now()));
            });
        }

        if (completedRaceRepository.count() == 0) {
            AtomicInteger placement = new AtomicInteger();
            Random random = new Random();
            registrationRepository.findAll().forEach(registeredUserSubEvent -> {
                CompletedRace completedRace = new CompletedRace();
                completedRace.setRegisteredRace(registeredUserSubEvent);

                if (placement.get() < registrationRepository.count() * 0.8) {
                    long hours = (placement.get() / (registrationRepository.count() / 3));
                    int minutes = random.nextInt(0, 60);
                    int seconds = random.nextInt(0, 60);
                    Duration timeTakenDuration = Duration.ofSeconds(hours * 3600L + minutes * 60L + seconds);
                    completedRace.setRunTime(timeTakenDuration);

                    completedRace.setFinished(true);

                    if (placement.get() < Medal.values().length)
                        completedRace.setMedal(Medal.values()[placement.get()]);
                    completedRace.setPlacement(placement.incrementAndGet());
                }
                completedRace.setParticipated(true);
                completedRaceRepository.saveAndFlush(completedRace);
            });
        }

        if (scoreAlgorithmService.findAll().isEmpty()) {
            try {
                scoreAlgorithmService.calculateAll();
            } catch (ResultsNotFoundException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
