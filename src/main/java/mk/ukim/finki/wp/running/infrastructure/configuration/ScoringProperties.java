package mk.ukim.finki.wp.running.infrastructure.configuration;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import lombok.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

@Value
@Validated
@ConfigurationProperties(prefix = "scoring")
public class ScoringProperties {
    @NotNull
    Integer maxPoints;

    @NotNull
    Integer baseParticipationPoints;

    @NotNull
    Integer finishingPoints;

    @Valid
    @NotNull
    MedalPoints medalPoints;

    @Valid
    @NotNull
    Penalties penalties;

    @NotNull
    Integer placementMaxPoints;

    @Value
    public static class MedalPoints {
        @NotNull
        Integer gold;

        @NotNull
        Integer silver;

        @NotNull
        Integer bronze;
    }

    @Value
    public static class Penalties {
        @NotNull
        Integer nonParticipation;
    }
}
