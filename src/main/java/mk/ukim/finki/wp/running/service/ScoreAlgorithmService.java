package mk.ukim.finki.wp.running.service;

import mk.ukim.finki.wp.running.model.domain.UserRaceScore;
import mk.ukim.finki.wp.running.model.exceptions.ResultsNotFoundException;
import mk.ukim.finki.wp.running.model.exceptions.ScoreNotFoundException;

import java.util.List;

public interface ScoreAlgorithmService {
    List<UserRaceScore> findAll();
    List<UserRaceScore> findAllByUserId(String userId);
    UserRaceScore findById(Long id);
    Double getUserScore(String userId);
    Double getUserScoreForRegistration(String userId, Long registrationId) throws ScoreNotFoundException;
    void calculateAll() throws ResultsNotFoundException;
    void calculate(Long registrationId) throws ResultsNotFoundException;
}
