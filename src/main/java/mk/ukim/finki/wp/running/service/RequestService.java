package mk.ukim.finki.wp.running.service;

import mk.ukim.finki.wp.running.model.domain.Request;
import mk.ukim.finki.wp.running.model.enumerations.RequestStatus;
import org.springframework.lang.Nullable;

import java.security.Principal;
import java.util.List;

public interface RequestService {
    List<Request> findAll();
    List<Request> findAllByStatus(RequestStatus status);
    List<Request> findAllByUserPrincipal(Principal principal);
    Request findById(Long id);
    Request create(Principal principal, Long subEventId, @Nullable String message);
    Request edit(Long id, Principal principal, Long subEventId, @Nullable String message);
    Request delete(Long id);

    Request changeStatus(Long id, String newStatus);
}
