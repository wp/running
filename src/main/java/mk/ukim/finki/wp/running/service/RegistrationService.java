package mk.ukim.finki.wp.running.service;

import mk.ukim.finki.wp.running.model.DTO.RaceParticipantsDto;
import mk.ukim.finki.wp.running.model.DTO.UpdateCompletedRaceDto;
import mk.ukim.finki.wp.running.model.domain.Registration;
import mk.ukim.finki.wp.running.model.domain.SubEvent;
import mk.ukim.finki.wp.running.model.domain.User;

import java.time.LocalDate;
import java.util.List;

public interface RegistrationService {
    List<Registration> findAll();

    List<Registration> findAllRegistrationsBySubEventId(Long id);

    List<RaceParticipantsDto> getRegistrationDtos(List<Registration> registrations);

    void coverRegistrationFee(Long id);

    void revokeCoveredRegistrationFee(Long id);

    Registration updateResults(Long id, UpdateCompletedRaceDto dto);

    Registration findById(Long id);

    List<Registration> findAllByUserId(String userId);

    Registration save(User user, SubEvent subEvent, LocalDate registryDate);

    Registration edit(Long id, User user, SubEvent subEvent, LocalDate registryDate);

    void deleteById(Long id);
    void deleteByUserIdAndSubEventId(String userId, Long subEventId);
}
