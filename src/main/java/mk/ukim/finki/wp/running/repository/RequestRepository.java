package mk.ukim.finki.wp.running.repository;

import mk.ukim.finki.wp.running.model.domain.Request;
import mk.ukim.finki.wp.running.model.enumerations.RequestStatus;
import mk.ukim.finki.wp.running.model.domain.SubEvent;
import mk.ukim.finki.wp.running.model.domain.User;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RequestRepository extends JpaRepository<Request, Long> {

    List<Request> findAllByUser(User user);

    List<Request> findAllByUserOrderByTimestampDesc(User user);

    List<Request> findAllByUser(User user, Pageable pageable);

    List<Request> findAllByStatus(RequestStatus status);

    List<Request> findAllByUserAndSubEvent(User user, SubEvent subEvent);

    List<Request> findAllBySubEvent_Id(Long subEventId);

    List<Request> findAllByUser_IdAndSubEvent_Id(String userId, Long subEventId);

}
