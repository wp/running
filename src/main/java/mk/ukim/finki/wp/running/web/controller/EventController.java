package mk.ukim.finki.wp.running.web.controller;

import lombok.RequiredArgsConstructor;
import mk.ukim.finki.wp.running.model.domain.Event;
import mk.ukim.finki.wp.running.service.EventService;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@Controller
@RequestMapping("/events")
@RequiredArgsConstructor
public class EventController {

    private final EventService eventService;

    @GetMapping("/all")
    public String getAllEvents(Model model, @RequestParam(required = false) String searchText) {
        List<Event> searchedEvents = null;
        if (searchText != null && !searchText.isBlank())
            searchedEvents = eventService.searchByName(searchText);

        List<Event> pastEvents = eventService.getPastEvents();
        List<Event> futureEvents = eventService.getFutureEvents();

        model.addAttribute("searched_events", searchedEvents);
        model.addAttribute("past_events", pastEvents);
        model.addAttribute("future_events", futureEvents);
        model.addAttribute("searchText", searchText);

        return "events/event-index";
    }

    @GetMapping("/details/{id}")
    public String getDetails(@PathVariable Long id, Model model) {
        model.addAttribute("event", this.eventService.findById(id));
        return "events/event-details";
    }

    @GetMapping("/add")
    public String getAddPage() {
        return "events/event-form";
    }

    @GetMapping("/edit/{id}")
    public String getEditPage(@PathVariable Long id, Model model) {
        model.addAttribute("event", this.eventService.findById(id));
        return "events/event-form";
    }

    @PostMapping("/save")
    public String saveEvent(@RequestParam String name,
                            @RequestParam String description,
                            @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime time,
                            @RequestParam String location
    ) {
        this.eventService.save(name, description, time, location);
        return "redirect:/events/all";
    }

    @PostMapping("/save/{id}")
    public String updateEvent(@PathVariable Long id,
                              @RequestParam String name,
                              @RequestParam String description,
                              @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime time,
                              @RequestParam String location) {
        this.eventService.edit(id, name, description, time, location);
        return "redirect:/events/all";
    }

    @PostMapping("/delete/{id}")
    public String deleteEvent(@PathVariable Long id) {
        this.eventService.deleteById(id);
        return "redirect:/events/all";
    }


}
