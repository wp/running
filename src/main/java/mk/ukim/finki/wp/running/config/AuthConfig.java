package mk.ukim.finki.wp.running.config;

import mk.ukim.finki.wp.running.model.enumerations.AppRole;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.LogoutConfigurer;

public class AuthConfig {

    public HttpSecurity authorize(HttpSecurity http) throws Exception {
        return http
                .csrf(csrf -> csrf.ignoringRequestMatchers("/api/**"))
                .authorizeHttpRequests((requests) -> requests
                        .requestMatchers(HttpMethod.OPTIONS).permitAll()
                        .requestMatchers(
                                "/admin/**",
                                "/events/add",
                                "/events/save/",
                                "/events/edit/*",
                                "/events/save/*",
                                "/events/delete/*",
                                "/results/registrations/update-results/*",
                                "/results/registrations/update-results/*",
                                "/results/registrations/revoke-covered-fee/*",
                                "/subEvents/all",
                                "/subEvents/add-form/*",
                                "/subEvents/edit/*",
                                "/subEvents/delete/*",
                                "/subEvents/save"
                        ).hasAnyRole(
                                AppRole.PROFESSOR.name(),
                                AppRole.ADMIN.name()
                        )
                        .requestMatchers("/api/**").hasAnyRole(AppRole.ADMIN.name())
                        .requestMatchers("io.png").permitAll()
                        .anyRequest().authenticated()
                )
                .logout(LogoutConfigurer::permitAll);
    }

}
