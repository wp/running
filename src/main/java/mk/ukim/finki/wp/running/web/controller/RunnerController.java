package mk.ukim.finki.wp.running.web.controller;

import lombok.RequiredArgsConstructor;
import mk.ukim.finki.wp.running.model.enumerations.Medal;
import mk.ukim.finki.wp.running.model.domain.Registration;
import mk.ukim.finki.wp.running.model.exceptions.ScoreNotFoundException;
import mk.ukim.finki.wp.running.service.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/")
@RequiredArgsConstructor
public class RunnerController {

    private final EventService eventService;
    private final RequestService requestService;
    private final RegistrationService registrationService;
    private final ScoreAlgorithmService scoreAlgorithmService;
    private final RunnerService runnerService;

    @GetMapping()
    public String index(Model model) {
        model.addAttribute("eventsMap", eventService.findAllUpcoming());
        return "runner/index";
    }

    @GetMapping("/user-requests")
    public String userRequests(Model model, Principal principal) {
        model.addAttribute("currentTime", LocalDateTime.now());
        model.addAttribute("requests", requestService.findAllByUserPrincipal(principal));
        return "runner/user-requests";
    }

    @GetMapping("/runners/{id}")
    public String getRunnerDetails(@PathVariable String id, Model model) {
        var user = runnerService.findById(id);
        model.addAttribute("user", user);

        model.addAttribute("id", id);
        model.addAttribute("totalScoreForUser", scoreAlgorithmService.getUserScore(id));

        List<Registration> registrations = registrationService.findAllByUserId(id);
        model.addAttribute("registrations", registrations);

        Map<Long, Double> scores = registrations
                .stream()
                .collect(Collectors.toMap(
                        r -> r.getId(),
                        r -> {
                            try {
                                return scoreAlgorithmService.getUserScoreForRegistration(id, r.getId());
                            } catch (ScoreNotFoundException e) {
                                return 0.0;
                            }
                        }
                ));
        model.addAttribute("scores", scores);

        var medals = runnerService.countMedalsByUserId(id);
        model.addAttribute("medalCountGold", medals.get(Medal.GOLD));
        model.addAttribute("medalCountSilver", medals.get(Medal.SILVER));
        model.addAttribute("medalCountBronze", medals.get(Medal.BRONZE));

        return "runner/user-details";
    }
}
