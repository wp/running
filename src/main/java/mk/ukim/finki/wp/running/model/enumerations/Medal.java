package mk.ukim.finki.wp.running.model.enumerations;

public enum Medal {
    GOLD,
    SILVER,
    BRONZE
}
