package mk.ukim.finki.wp.running.service;

import mk.ukim.finki.wp.running.model.domain.Event;
import mk.ukim.finki.wp.running.model.domain.SubEvent;
import mk.ukim.finki.wp.running.model.enumerations.SubEventLength;

import java.time.LocalDateTime;
import java.util.List;

public interface SubEventService {
    List<SubEvent> findAll();
    List<SubEvent> findAllByEventId(Long eventId);

    List<SubEvent> searchByName(String name);

    SubEvent findById(Long id);

    SubEvent save(String name, SubEventLength length, String imageUrl, String startInfo, Long eventId, LocalDateTime startTime);

    SubEvent edit(Long id, String name, SubEventLength length, String imageUrl, String startInfo, Long eventId, LocalDateTime startTime);

    void delete(Long id);
    List<SubEvent> findAllUpcomingByEvent(Event event);
    String convertLength(SubEventLength length);
}
