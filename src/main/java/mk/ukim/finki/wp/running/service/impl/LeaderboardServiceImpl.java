package mk.ukim.finki.wp.running.service.impl;

import lombok.RequiredArgsConstructor;
import mk.ukim.finki.wp.running.model.DTO.RegisteredUserDto;
import mk.ukim.finki.wp.running.model.domain.User;
import mk.ukim.finki.wp.running.repository.RegistrationRepository;
import mk.ukim.finki.wp.running.service.LeaderboardService;
import mk.ukim.finki.wp.running.service.ScoreAlgorithmService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Service
@RequiredArgsConstructor
public class LeaderboardServiceImpl implements LeaderboardService {

    private final RegistrationRepository registrationRepository;

    private final ScoreAlgorithmService scoreAlgorithmService;

    @Override
    public List<RegisteredUserDto> getAllRegisteredUsersWithTotalScores(String sort) {
        List<User> users = registrationRepository.findAllDistinctUsers();
        List<RegisteredUserDto> registeredUserDtos = populateData(users);

        sortRegisteredUsers(registeredUserDtos, sort);

        return registeredUserDtos;
    }

    @Override
    public List<RegisteredUserDto> findAllByEvent(Long eventId, String sort) {
        List<User> users = registrationRepository.findDistinctUsersByEventId(eventId);
        List<RegisteredUserDto> registeredUserDtos = populateData(users);

        sortRegisteredUsers(registeredUserDtos, sort);

        return registeredUserDtos;
    }

    private List<RegisteredUserDto> populateData(List<User> users) {
        List<RegisteredUserDto> registeredUserDtos = new ArrayList<>();

        users.forEach(user -> {
            Double totalScore = scoreAlgorithmService.getUserScore(user.getId());
            RegisteredUserDto dto = new RegisteredUserDto(user.getId(), user.getName(), totalScore);
            registeredUserDtos.add(dto);
        });

        return registeredUserDtos;
    }

    private void sortRegisteredUsers(List<RegisteredUserDto> registeredUserDtos, String sort) {
        if (sort != null && sort.equals("asc")) {
            registeredUserDtos.sort(Comparator.comparingDouble(RegisteredUserDto::getTotalScore));

            int rank = registeredUserDtos.size();
            for (RegisteredUserDto registeredUserDto : registeredUserDtos) {
                registeredUserDto.setRanking(rank--);
            }
        } else {
            registeredUserDtos.sort(Comparator.comparingDouble(RegisteredUserDto::getTotalScore).reversed());

            int rank = 1;
            registeredUserDtos.sort(Comparator.comparingDouble(RegisteredUserDto::getTotalScore).reversed());
            for (RegisteredUserDto registeredUserDto : registeredUserDtos) {
                registeredUserDto.setRanking(rank++);
            }
        }
    }
}
