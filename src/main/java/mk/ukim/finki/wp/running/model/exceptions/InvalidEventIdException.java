package mk.ukim.finki.wp.running.model.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class InvalidEventIdException extends RuntimeException{
    public InvalidEventIdException(Long id){
        super("Event with id " + id + " not found");
    }
}
