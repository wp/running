package mk.ukim.finki.wp.running.web.controller;

import lombok.RequiredArgsConstructor;
import mk.ukim.finki.wp.running.model.DTO.UpdateCompletedRaceDto;
import mk.ukim.finki.wp.running.model.enumerations.Medal;
import mk.ukim.finki.wp.running.service.RegistrationService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.time.Duration;

@Controller
@RequestMapping("/results/registrations")
@RequiredArgsConstructor
public class RegistrationController {

    private final RegistrationService userRegistrationService;
    @GetMapping("/{id}")
    public String getCompletedRaceInfo(@PathVariable Long id, Model model) {
        var registration = userRegistrationService.findById(id);
        model.addAttribute("registration", registration);
        model.addAttribute("medals", Medal.values());

        if (registration.getCompletedRace() != null) {
            var duration = registration.getCompletedRace().getRunTime();
            if (duration == null) duration = Duration.ZERO;
            model.addAttribute("hr", duration.toHours());
            model.addAttribute("min", duration.toMinutesPart());
            model.addAttribute("sec", duration.toSecondsPart());
        }

        return "/subEvents/update-participant-results";
    }

    @PostMapping("/update-results/{id}")
    public String updateCompletedRaceUserResults(
            @ModelAttribute UpdateCompletedRaceDto dto,
            @PathVariable Long id
    ) {
        userRegistrationService.updateResults(id, dto);

        var registration = userRegistrationService.findById(id);
        return "redirect:/subEvents/details/" + registration.getSubEvent().getId();
    }

    @GetMapping("/cover-fee/{id}")
    public String coverRegistrationFee(@PathVariable Long id) {
        userRegistrationService.coverRegistrationFee(id);

        var registration = userRegistrationService.findById(id);
        return "redirect:/subEvents/details/" + registration.getSubEvent().getId();
    }

    @GetMapping("revoke-covered-fee/{id}")
    public String revokeCoveredRegistrationFee(@PathVariable Long id) {
        userRegistrationService.revokeCoveredRegistrationFee(id);

        var registration = userRegistrationService.findById(id);
        return "redirect:/subEvents/details/" + registration.getSubEvent().getId();
    }
}
