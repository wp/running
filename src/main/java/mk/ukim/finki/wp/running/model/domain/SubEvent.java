package mk.ukim.finki.wp.running.model.domain;

import jakarta.annotation.Nullable;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import mk.ukim.finki.wp.running.model.enumerations.SubEventLength;

import java.time.LocalDateTime;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SubEvent {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long Id;

    private String name;

    @Enumerated(value = EnumType.STRING)
    private SubEventLength length;

    @Nullable
    @Column(length = 1000)
    private String imageUrl;

    @Nullable
    private String startInfo;

    @ManyToOne
    @ToString.Exclude
    private Event event;

    @Nullable
    private LocalDateTime startTime;

    public SubEvent(String name, SubEventLength length, @Nullable String imageUrl, @Nullable String startInfo, LocalDateTime startTime) {
        this.name = name;
        this.length = length;
        this.imageUrl = imageUrl;
        this.startInfo = startInfo;
        this.startTime = startTime;
    }

    public String getLengthString(){
        return switch (length) {
            case FIVE -> "5km";
            case TEN -> "10km";
            case HALF_MARATHON -> "Полу-Маратон (21km)";
            case FULL_MARATHON -> "Маратон (42km)";
            case FULL_MARATHON_RELAY -> "Маратон - Штафета (42km)";
        };
    }
}
