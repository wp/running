package mk.ukim.finki.wp.running.infrastructure.utils;

import jakarta.annotation.Nullable;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.time.Duration;

@Component
public class DurationConverter implements Converter<Duration, String> {

    @Override
    public String convert(@Nullable Duration source) {
        if (source == null) return null;

        long hours = source.toHours();
        long minutes = source.toMinutesPart();
        long seconds = source.toSecondsPart();

        return String.format("%02d:%02d:%02d", hours, minutes, seconds);
    }
}