package mk.ukim.finki.wp.running.repository;

import mk.ukim.finki.wp.running.model.domain.Event;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;

public interface EventRepository extends JpaRepository<Event, Long> {
    Event findByName(String name);
    List<Event> findAllByNameContainingIgnoreCase(String name);
    List<Event> findAllByTimeBefore(LocalDateTime current);
    List<Event> findAllByTimeAfter(LocalDateTime time);
}
