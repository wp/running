package mk.ukim.finki.wp.running.web.controller;

import lombok.RequiredArgsConstructor;
import mk.ukim.finki.wp.running.service.LeaderboardService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/leaderboard")
@RequiredArgsConstructor
public class LeaderboardController {

    private final LeaderboardService leaderboardService;

    @GetMapping
    public String getLeaderboardView(Model model, @RequestParam(required = false) String sort) {
        model.addAttribute("registeredUsers", leaderboardService.getAllRegisteredUsersWithTotalScores(sort));

        return "leaderboard/leaderboard-list";
    }

    @GetMapping("/{eventId}")
    public String getEventLeaderboardView(Model model, @PathVariable Long eventId,
                                          @RequestParam(required = false) String sort) {
        model.addAttribute("eventUsers", leaderboardService.findAllByEvent(eventId, sort));
        model.addAttribute("eventId", eventId);

        return "leaderboard/leaderboard-event-list";
    }
}
