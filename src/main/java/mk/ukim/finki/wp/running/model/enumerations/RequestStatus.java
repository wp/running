package mk.ukim.finki.wp.running.model.enumerations;

public enum RequestStatus {
    PENDING, REJECTED, APPROVED
}
