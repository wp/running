package mk.ukim.finki.wp.running.service;

import mk.ukim.finki.wp.running.model.DTO.RegisteredUserDto;

import java.util.List;

public interface LeaderboardService {
    List<RegisteredUserDto> getAllRegisteredUsersWithTotalScores(String sort);

    List<RegisteredUserDto> findAllByEvent(Long eventId, String sort);
}
