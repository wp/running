package mk.ukim.finki.wp.running.web.controller;

import lombok.RequiredArgsConstructor;
import mk.ukim.finki.wp.running.model.enumerations.RequestStatus;
import mk.ukim.finki.wp.running.service.RequestService;
import mk.ukim.finki.wp.running.service.SubEventService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/admin")
@RequiredArgsConstructor
public class AdminController {
    private final SubEventService subEventService;
    private final RequestService requestService;

    @GetMapping("/requests")
    public String showRequests(@RequestParam(value = "status", required = false) String status, Model model) {

        if (status != null) {
            RequestStatus requestStatus;
            try {
                requestStatus = RequestStatus.valueOf(status.toUpperCase());
            } catch (IllegalArgumentException e) {
                requestStatus = null;
            }

            if (requestStatus == RequestStatus.PENDING) {
                model.addAttribute("title", "Активни апликации");
                model.addAttribute("requests", requestService.findAllByStatus(RequestStatus.PENDING));
            }
            if (requestStatus == RequestStatus.APPROVED) {
                model.addAttribute("title", "Одобрени апликации");
                model.addAttribute("requests", requestService.findAllByStatus(RequestStatus.APPROVED));
            }
            if (requestStatus == RequestStatus.REJECTED) {
                model.addAttribute("title", "Одбиени апликации");
                model.addAttribute("requests", requestService.findAllByStatus(RequestStatus.REJECTED));
            }
            return "request/list";
        }

        model.addAttribute("title", "Сите апликации");
        model.addAttribute("requests", requestService.findAll());

        return "request/list";

    }


    @GetMapping("/request/resolve/{id}")
    public String resolveRequest(@PathVariable Long id, Model model) {
        model.addAttribute("request", requestService.findById(id));
        return "request/resolve";
    }

    @PostMapping("/request/resolve/{id}")
    public String deleteRequest(@PathVariable Long id, @RequestParam String statusUpdate) {
        this.requestService.changeStatus(id, statusUpdate);
        return "redirect:/admin/requests?status=PENDING";
    }


}
