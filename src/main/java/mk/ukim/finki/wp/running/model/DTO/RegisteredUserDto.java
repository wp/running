package mk.ukim.finki.wp.running.model.DTO;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class RegisteredUserDto {
    private String userId;

    private String userName;

    private int ranking;

    private Double totalScore;

    public RegisteredUserDto(String userId, String userName, Double totalScore) {
        this.userId = userId;
        this.userName = userName;
        this.totalScore = totalScore;
    }
}
