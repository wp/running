package mk.ukim.finki.wp.running.model.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class InvalidSubEventIdException extends RuntimeException{

    public InvalidSubEventIdException(Long id){
        super("Invalid sub event id " + id);
    }
}
