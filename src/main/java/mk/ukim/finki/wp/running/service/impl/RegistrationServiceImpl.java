package mk.ukim.finki.wp.running.service.impl;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import mk.ukim.finki.wp.running.model.DTO.RaceParticipantsDto;
import mk.ukim.finki.wp.running.model.DTO.UpdateCompletedRaceDto;
import mk.ukim.finki.wp.running.model.domain.SubEvent;
import mk.ukim.finki.wp.running.model.domain.User;
import mk.ukim.finki.wp.running.model.domain.*;
import mk.ukim.finki.wp.running.model.enumerations.Medal;
import mk.ukim.finki.wp.running.model.exceptions.RegistrationNotFoundException;
import mk.ukim.finki.wp.running.model.exceptions.ResultsNotFoundException;
import mk.ukim.finki.wp.running.repository.CompletedRaceRepository;
import mk.ukim.finki.wp.running.repository.FeeDiscountRepository;
import mk.ukim.finki.wp.running.repository.RegistrationRepository;
import mk.ukim.finki.wp.running.repository.VoucherRepository;
import mk.ukim.finki.wp.running.service.ScoreAlgorithmService;
import mk.ukim.finki.wp.running.service.RegistrationService;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class RegistrationServiceImpl implements RegistrationService {

    private final RegistrationRepository registrationRepository;
    private final VoucherRepository voucherRepository;
    private final CompletedRaceRepository completedRaceRepository;
    private final FeeDiscountRepository feeDiscountRepository;
    private final ScoreAlgorithmService scoreAlgorithmService;

    public List<Registration> findAll() {
        return registrationRepository.findAll();
    }

    @Override
    public List<Registration> findAllRegistrationsBySubEventId(Long id) {
        return registrationRepository.findAllBySubEventId(id);
    }

    double findUserTotalScore(String id) {
        return scoreAlgorithmService.getUserScore(id);
    }

    @Override
    public List<RaceParticipantsDto> getRegistrationDtos(List<Registration> registrations) {
        List<RaceParticipantsDto> registrationDTOs = new ArrayList<>();
        for(var registration : registrations) {
            var registrationDto = new RaceParticipantsDto(
                    registration.getId(),
                    registration.getSubEvent().getEvent(),
                    registration.getUser(),
                    registration.getRegistryDate(),
                    findUserTotalScore(registration.getUser().getId()),
                    registration.getFeeDiscount(),
                    registration.getCompletedRace()
            );
            registrationDTOs.add(registrationDto);
        }
        return registrationDTOs;
    }

    @Override
    public void coverRegistrationFee(Long id) {
        var registration = findById(id);
        var feeDiscount = new FeeDiscount();
        registration.setFeeDiscount(feeDiscount);
        feeDiscountRepository.save(feeDiscount);
        registrationRepository.save(registration);
    }

    @Override
    public void revokeCoveredRegistrationFee(Long id) {
        var registration = findById(id);
        registration.setFeeDiscount(null);
        registrationRepository.save(registration);
    }

    @Override
    public Registration updateResults(Long id, UpdateCompletedRaceDto dto) {
        var subEventUserRegistration = findById(id);
        CompletedRace completedRace;

        var participated = dto.isParticipated();
        var finished = dto.isFinished();
        var elapsedTimeInSeconds = dto.getSec() + dto.getMin() * 60 + dto.getHr() * 3600;
        var placement = dto.getPlacement();
        var medal = dto.getMedal().equals("None") ? null : Medal.valueOf(dto.getMedal());
        Voucher voucher = null;
        if (dto.isVoucher()) {
            voucher = new Voucher();
            voucherRepository.save(voucher);
        }

        Duration duration = Duration.ofSeconds(elapsedTimeInSeconds);

        if (subEventUserRegistration.getCompletedRace() != null) {
            completedRace = subEventUserRegistration.getCompletedRace();
            completedRace.setFinished(finished);
            completedRace.setParticipated(participated);
            completedRace.setRunTime(duration);
            completedRace.setPlacement(placement);
            completedRace.setMedal(medal);
            completedRace.setVoucher(voucher);
        } else {
            completedRace = new CompletedRace(participated, finished, duration, placement, medal, voucher);
            completedRace.setRegisteredRace(subEventUserRegistration);
        }

        subEventUserRegistration.setCompletedRace(completedRace);
        completedRaceRepository.save(completedRace);
        var registration = registrationRepository.save(subEventUserRegistration);

        // Calculate score for registration
        try {
            scoreAlgorithmService.calculate(registration.getId());
        } catch (ResultsNotFoundException e) {
            System.err.println(e.getMessage());
        }

        return registration;
    }

    @Override
    public Registration findById(Long id) {
        return registrationRepository.findById(id)
                .orElseThrow(() -> new RegistrationNotFoundException(id));
    }

    public List<Registration> findAllByUserId(String userId) {
        return registrationRepository.findAllByUserId(userId);
    }

    @Override
    public Registration save(User user, SubEvent subEvent, LocalDate registryDate) {
        Registration Registration = new Registration(user, subEvent, registryDate);
        return registrationRepository.save(Registration);
    }

    @Override
    public Registration edit(Long id, User user, SubEvent subEvent, LocalDate registryDate) {
        Registration Registration = registrationRepository.findById(id)
                .orElseThrow(() -> new RegistrationNotFoundException(id));

        Registration.setUser(user);
        Registration.setSubEvent(subEvent);
        Registration.setRegistryDate(registryDate);

        return registrationRepository.save(Registration);
    }

    @Override
    public void deleteById(Long id) {
        registrationRepository.deleteById(id);
    }

    @Transactional
    @Override
    public void deleteByUserIdAndSubEventId(String userId, Long subEventId) {
        registrationRepository.deleteByUserIdAndSubEventId(userId, subEventId);
    }
}
