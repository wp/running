package mk.ukim.finki.wp.running.model.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND)
public class ScoreNotFoundException extends RuntimeException {
    public ScoreNotFoundException() {
        super("Score not found");
    }

    public ScoreNotFoundException(Long id) {
        super(String.format("Score with the id %d not found", id));
    }

    public ScoreNotFoundException(String userId, Long registrationId) {
        super(String.format("Score for the user %s registration %d not found", userId, registrationId));
    }
}
