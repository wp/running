package mk.ukim.finki.wp.running.service.impl;

import lombok.Data;
import mk.ukim.finki.wp.running.infrastructure.configuration.ScoringProperties;
import mk.ukim.finki.wp.running.model.domain.CompletedRace;
import mk.ukim.finki.wp.running.model.domain.Registration;
import mk.ukim.finki.wp.running.model.domain.UserRaceScore;
import mk.ukim.finki.wp.running.model.exceptions.RegistrationNotFoundException;
import mk.ukim.finki.wp.running.model.exceptions.ResultsNotFoundException;
import mk.ukim.finki.wp.running.model.exceptions.ScoreNotFoundException;
import mk.ukim.finki.wp.running.repository.RegistrationRepository;
import mk.ukim.finki.wp.running.repository.UserScoreRepository;
import mk.ukim.finki.wp.running.service.RegistrationService;
import mk.ukim.finki.wp.running.service.ScoreAlgorithmService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ScoreAlgorithmServiceImpl implements ScoreAlgorithmService {
    private final RegistrationRepository registrationRepository;
    private final UserScoreRepository userScoreRepository;
    private final ScoringProperties scoringProperties;

    public ScoreAlgorithmServiceImpl(RegistrationRepository registrationRepository, UserScoreRepository userScoreRepository, ScoringProperties scoringProperties) {
        this.registrationRepository = registrationRepository;
        this.userScoreRepository = userScoreRepository;
        this.scoringProperties = scoringProperties;
    }

    @Override
    public List<UserRaceScore> findAll() {
        return this.userScoreRepository.findAll();
    }

    @Override
    public List<UserRaceScore> findAllByUserId(String userId) {
        return this.userScoreRepository.findAllByUserId(userId);
    }

    @Override
    public UserRaceScore findById(Long id) {
        return this.userScoreRepository
                .findById(id)
                .orElseThrow(() -> new ScoreNotFoundException(id));
    }

    @Override
    public Double getUserScore(String userId) {
        var scores = this.userScoreRepository.findAllByUserId(userId);

        double average_score = scores.stream()
                .mapToDouble(UserRaceScore::getScore)
                .filter(score -> score > 0)
                .average()
                .orElse(0.0);

        double penalties_sum = scores.stream().mapToDouble(UserRaceScore::getPenalty).sum();

        // Apply the penalties
        penalties_sum = Math.min(penalties_sum, 25.0);
        double score = average_score - penalties_sum;

        // Clamp the score
        score = clamp(score, 0, 100);

        score = Math.round(score * 100.0) / 100.0;

        return score;
    }
    @Override
    public Double getUserScoreForRegistration(String userId, Long registrationId) {
        var userRaceScore = this.userScoreRepository
                .findByRegistrationId(registrationId)
                .orElseThrow(() -> new ScoreNotFoundException(userId, registrationId));

        // Return penalty if found
        if (userRaceScore.getPenalty() > 0) {
            return userRaceScore.getPenalty() * -1;
        }

        return userRaceScore.getScore();
    }

    public void calculateAll() throws ResultsNotFoundException {
        for (var registration : this.registrationRepository.findAll()) {
            calculate(registration.getId());
        }
    }

    @Override
    public void calculate(Long registrationId) throws ResultsNotFoundException {
        Registration registration = this.registrationRepository
                .findById(registrationId)
                .orElseThrow(() -> new RegistrationNotFoundException(registrationId));

        var completedRace = registration.getCompletedRace();

        if (completedRace == null) {
            throw new ResultsNotFoundException();
        }

        var result = calculatePointsAndPenalties(completedRace);
        var points = result.getPoints();
        var penalties = result.getPenalties();

        // Calculate final score
        var score = calculateScore(points, penalties);

        // Save score
        UserRaceScore entry = this.userScoreRepository
                .findByRegistrationId(registrationId)
                .orElse(null);

        if (entry != null) {
            entry.setPoints(points);
            entry.setPenalty(penalties);
            entry.setScore(score);
        } else {
            entry = new UserRaceScore(
                    registration.getUser(),
                    registration,
                    points,
                    penalties,
                    score,
                    registration.getSubEvent().getEvent().getTime()
            );
        }

        userScoreRepository.save(entry);
    }

    @Data
    class PointsCalculationResult {
        private final double points;
        private final double penalties;
    }

    PointsCalculationResult calculatePointsAndPenalties(CompletedRace completedRace) {
        double points = 0.0;
        double penalties = 0.0;

        // Check if participated
        if (completedRace.isParticipated()) {
            points += scoringProperties.getBaseParticipationPoints();
        } else {
            penalties += scoringProperties.getPenalties().getNonParticipation();
        }

        // Check if finished
        if (completedRace.isFinished()) {
            points += scoringProperties.getFinishingPoints();
        }

        // Handle medal
        if (completedRace.getMedal() != null) {
            switch(completedRace.getMedal()) {
                case GOLD -> points += scoringProperties.getMedalPoints().getGold();
                case SILVER -> points += scoringProperties.getMedalPoints().getSilver();
                case BRONZE -> points += scoringProperties.getMedalPoints().getBronze();
            }
        }

        // Handle placement
        if (completedRace.getPlacement() > 0) {
            points += (double) scoringProperties.getPlacementMaxPoints() / Math.log(completedRace.getPlacement() + 1);
        }

        points = roundToTwoDecimals(points);

        // If user has penalties they shouldn't have points
        if (penalties > 0) {
            points = 0;
        }

        return new PointsCalculationResult(points, penalties);
    }

    double calculateScore(double points, double penalties) {
        var score = points / scoringProperties.getMaxPoints() * 100;
        score = clamp(roundToTwoDecimals(score), 0, 100);
        return score;
    }

    double roundToTwoDecimals(double value) {
        return Math.round(value * 100.0) / 100.0;
    }

    double clamp(double value, double min, double max) {
        return Math.max(min, Math.min(value, max));
    }
}
