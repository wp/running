package mk.ukim.finki.wp.running.model.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class EventNotFoundException extends RuntimeException{
    public EventNotFoundException(Long id) {
        super(String.format("The event with the id: %d doesn't exist", id));
    }
}
