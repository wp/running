package mk.ukim.finki.wp.running.model.enumerations;

public enum AppRole {
    ADMIN, PROFESSOR, STUDENT, GUEST;

    public String roleName() {
        return "ROLE_" + this.name();
    }
}
