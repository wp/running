package mk.ukim.finki.wp.running.model.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@NoArgsConstructor
@Entity
@Data
@Table(
        uniqueConstraints = {
                @UniqueConstraint(columnNames = {"user_id", "sub_event_id"})
        }
)
public class Registration {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    private User user;

    @ManyToOne
    private SubEvent subEvent;
    private LocalDate registryDate;

    @OneToOne
    private FeeDiscount feeDiscount;

    @OneToOne(mappedBy = "registeredRace")
    @JsonManagedReference
    private CompletedRace completedRace;

    public Registration(User user, SubEvent subEvent, LocalDate registryDate) {
        this.user = user;
        this.subEvent = subEvent;
        this.registryDate = registryDate;
    }
}