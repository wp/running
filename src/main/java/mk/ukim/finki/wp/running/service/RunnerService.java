package mk.ukim.finki.wp.running.service;

import mk.ukim.finki.wp.running.model.domain.User;
import mk.ukim.finki.wp.running.model.enumerations.Medal;

import java.util.Map;

public interface RunnerService {
    User findById(String userId);
    Map<Medal, Long> countMedalsByUserId(String userId);
}
