package mk.ukim.finki.wp.running.repository;

import mk.ukim.finki.wp.running.model.domain.UserRaceScore;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserScoreRepository extends JpaSpecificationRepository<UserRaceScore, Long> {
    List<UserRaceScore> findAllByUserId(String userId);

    Optional<UserRaceScore> findByRegistrationId(Long registrationId);
}
