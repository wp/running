package mk.ukim.finki.wp.running.service.impl;

import lombok.RequiredArgsConstructor;
import mk.ukim.finki.wp.running.model.domain.Event;
import mk.ukim.finki.wp.running.model.domain.SubEvent;
import mk.ukim.finki.wp.running.model.exceptions.EventNotFoundException;
import mk.ukim.finki.wp.running.model.exceptions.SubEventNotFoundException;
import mk.ukim.finki.wp.running.repository.EventRepository;
import mk.ukim.finki.wp.running.repository.SubEventRepository;
import mk.ukim.finki.wp.running.service.EventService;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class EventServiceImpl implements EventService {

    private final EventRepository eventRepository;
    private final SubEventRepository subEventRepository;


    @Override
    public List<Event> findAll() {
        return this.eventRepository.findAll();
    }

    @Override
    public List<Event> getFutureEvents() {
        return eventRepository.findAllByTimeAfter(LocalDateTime.now());
    }

    @Override
    public List<Event> getPastEvents() {
        return eventRepository.findAllByTimeBefore(LocalDateTime.now());
    }

    @Override
    public List<Event> findAllByName(String name) {
        return this.eventRepository.findAllByNameContainingIgnoreCase(name);
    }

    @Override
    public List<Event> searchByName(String name) {
        return eventRepository.findAllByNameContainingIgnoreCase(name);
    }

    @Override
    public Event findById(Long id) {
        return this.eventRepository.findById(id).orElseThrow(() -> new EventNotFoundException(id));
    }

    @Override
    public Event findByName(String name) {
        return this.eventRepository.findByName(name);
    }

    @Override
    public void save(String name, String description, LocalDateTime time, String location) {
        Event event = new Event(name, description, time, location);
        this.eventRepository.save(event);
    }

    @Override
    public void edit(Long id, String name, String description, LocalDateTime time, String location) {
        Event event = this.findById(id);
        event.setName(name);
        event.setDescription(description);
        event.setTime(time);
        event.setLocation(location);

        this.eventRepository.save(event);

    }

    @Override
    public void deleteById(Long id) {
        Event event = this.findById(id);

        this.eventRepository.delete(event);
    }

    @Override
    public void addSubEventToEvent(Long eventId, Long subEventId) {
        Event event = this.findById(eventId);

        SubEvent subEvent = this.subEventRepository.findById(subEventId).orElseThrow(() -> new SubEventNotFoundException(subEventId));
        event.getSubEvents().add(subEvent);

        this.eventRepository.save(event);

    }

    @Override
    public void removeSubEventFromEvent(Long eventId, Long subEventId) {
        Event event = this.findById(eventId);

        SubEvent subEvent = this.subEventRepository.findById(subEventId).orElseThrow(() -> new SubEventNotFoundException(subEventId));
        event.getSubEvents().remove(subEvent);

        this.eventRepository.save(event);
    }

    @Override
    public Map<Event, List<SubEvent>> findAllUpcoming() {
        Map<Event, List<SubEvent>> map = new HashMap<>();

        List<Event> events = eventRepository.findAllByTimeAfter(LocalDateTime.now());

        events.forEach(event -> {
            List<SubEvent> subevents = subEventRepository.findSubEventsByEventAndStartTimeAfter(event, LocalDateTime.now());
            map.put(event, subevents);
        });

        return map;
    }


}
