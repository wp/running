package mk.ukim.finki.wp.running.model.domain;

import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
public class Event {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String description;
    private LocalDateTime time;
    private String location;


    @OneToMany(mappedBy = "event", cascade = CascadeType.ALL)
    private List<SubEvent> subEvents;

    public Event(String name, String description, LocalDateTime time, String location) {
        this.name = name;
        this.description = description;
        this.time = time;
        this.location = location;
        this.subEvents = new ArrayList<>();

    }
}
