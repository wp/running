package mk.ukim.finki.wp.running.repository;

import mk.ukim.finki.wp.running.model.domain.Event;
import mk.ukim.finki.wp.running.model.domain.SubEvent;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;

public interface SubEventRepository extends JpaRepository<SubEvent, Long> {
    List<SubEvent> findAllByEventId(Long eventId);
    List<SubEvent> findSubEventsByNameContaining(String subEventName);
    List<SubEvent> findSubEventsByEventAndStartTimeAfter(Event event, LocalDateTime startTime);
}
