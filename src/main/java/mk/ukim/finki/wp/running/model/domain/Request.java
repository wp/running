package mk.ukim.finki.wp.running.model.domain;

import jakarta.annotation.Nullable;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import mk.ukim.finki.wp.running.model.enumerations.RequestStatus;

import java.time.LocalDateTime;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "subevent_request")
public class Request {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long Id;

    @ManyToOne
    private User user;

    @ManyToOne
    private SubEvent subEvent;

    @Nullable
    @Column(length = 1000)
    private String message;

    private RequestStatus status;

    private LocalDateTime timestamp;

    public Request(User user, SubEvent subEvent, @Nullable String message) {
        this.user = user;
        this.subEvent = subEvent;
        this.message = message;
        this.status = RequestStatus.PENDING;
        //this.status = user.getRole()==UserRole.PROFESSOR?RequestStatus.APPROVED:RequestStatus.PENDING;
        this.timestamp = LocalDateTime.now();
    }
}
