package mk.ukim.finki.wp.running.web.controller;

import lombok.RequiredArgsConstructor;
import mk.ukim.finki.wp.running.model.domain.SubEvent;
import mk.ukim.finki.wp.running.model.enumerations.SubEventLength;
import mk.ukim.finki.wp.running.service.EventService;
import mk.ukim.finki.wp.running.service.RegistrationService;
import mk.ukim.finki.wp.running.service.SubEventService;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@Controller
@RequestMapping("/subEvents")
@RequiredArgsConstructor
public class SubEventController {
    private final SubEventService subEventService;
    private final EventService eventService;
    private final RegistrationService registrationService;

    @GetMapping("/all")
    public String getAllSubEvents(Model model, @RequestParam(required = false) String searchText) {

        if (searchText != null) {
            model.addAttribute("searchText", searchText);

            List<SubEvent> subEvents = this.subEventService.searchByName(searchText);
            model.addAttribute("subEvents", subEvents);
        } else {
            List<SubEvent> events = this.subEventService.findAll();

            model.addAttribute("subEvents", events);
        }

        return "subEvents/subEvent-index";
    }

    @GetMapping("/details/{id}")
    public String getDetails(@PathVariable Long id, Model model) {
        SubEvent se = subEventService.findById(id);
        model.addAttribute("subEvent", se);
        model.addAttribute("length", subEventService.convertLength(se.getLength()));


        // Participants
        var participantRegistrations = registrationService.findAllRegistrationsBySubEventId(id);
        boolean isFinished = se.getEvent().getTime().plusHours(2).isBefore(LocalDateTime.now());
        var registrations = registrationService.getRegistrationDtos(participantRegistrations);

        model.addAttribute("isEventFinished", isFinished);
        model.addAttribute("registrations", registrations);

        return "subEvents/subEvent-details";
    }

    @GetMapping("/add-form/{eventId}")
    public String addSubEventPage(@PathVariable Long eventId, Model model) {
        model.addAttribute("event", eventService.findById(eventId));
        model.addAttribute("lengths", SubEventLength.values());
        return "subEvents/subEvent-form";
    }

    @PostMapping("/save")
    public String saveSubEvent(@RequestParam String name,
                               @RequestParam SubEventLength length,
                               @RequestParam String imageUrl,
                               @RequestParam String info,
                               @RequestParam Long eventId,
                               @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime startTime) {
        this.subEventService.save(name, length, imageUrl, info, eventId, startTime);
        return "redirect:/events/details/" + eventId;
    }

    @GetMapping("/edit/{id}")
    public String editSubEventPage(@PathVariable Long id, Model model) {
        SubEvent subEvent = subEventService.findById(id);
        model.addAttribute("subEvent", subEvent);
        model.addAttribute("event", subEvent.getEvent());
        model.addAttribute("lengths", SubEventLength.values());
        return "subEvents/edit-form";
    }

    @PostMapping("/edit/{id}")
    public String editSubEvent(@PathVariable Long id,
                               @RequestParam String name,
                               @RequestParam SubEventLength length,
                               @RequestParam String imageUrl,
                               @RequestParam String info,
                               @RequestParam Long eventId,
                               @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime startTime) {
        this.subEventService.edit(id, name, length, imageUrl, info, eventId, startTime);
        return "redirect:/events/details/" + eventId;
    }

    @PostMapping("/delete/{id}")
    public String deleteSubEvent(@PathVariable Long id) {
        Long eventId = this.subEventService.findById(id).getEvent().getId();
        this.subEventService.delete(id);
        return "redirect:/events/details/" + eventId;
    }


}
