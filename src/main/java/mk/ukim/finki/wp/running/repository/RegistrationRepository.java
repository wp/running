package mk.ukim.finki.wp.running.repository;

import mk.ukim.finki.wp.running.model.domain.Registration;
import mk.ukim.finki.wp.running.model.domain.SubEvent;
import mk.ukim.finki.wp.running.model.domain.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RegistrationRepository extends JpaSpecificationRepository<Registration, Long> {
    List<Registration> findAllBySubEvent(SubEvent subEvent);

    @Query("select distinct re.user from Registration as re")
    List<User> findAllDistinctUsers();

    @Query("select distinct re.user from Registration re " +
            "left join re.subEvent se " +
            "left join se.event e " +
            "where e.id = :eventId")
    List<User> findDistinctUsersByEventId(@Param("eventId") Long eventId);

    List<Registration> findAllBySubEventId(Long id);
    List<Registration> findAllByUserId(String userId);
    void deleteByUserIdAndSubEventId(String userId, Long subEventId);
}
