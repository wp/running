package mk.ukim.finki.wp.running.model.exceptions;

public class ResultsNotFoundException extends Exception {
    public ResultsNotFoundException() {
        super("The results for the registration were not found");
    }
}
