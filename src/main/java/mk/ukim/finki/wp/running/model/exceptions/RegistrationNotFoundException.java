package mk.ukim.finki.wp.running.model.exceptions;

public class RegistrationNotFoundException extends RuntimeException {
    public RegistrationNotFoundException(Long id) {
        super(String.format("Registration with id: %d was not found", id));
    }
}

