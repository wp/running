package mk.ukim.finki.wp.running.model.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import mk.ukim.finki.wp.running.model.domain.CompletedRace;
import mk.ukim.finki.wp.running.model.domain.Event;
import mk.ukim.finki.wp.running.model.domain.FeeDiscount;
import mk.ukim.finki.wp.running.model.domain.User;

import java.time.LocalDate;

@Data
@AllArgsConstructor
public class RaceParticipantsDto {
    private Long id;
    private Event event;
    private User user;
    private LocalDate registryDate;
    private double score;
    private FeeDiscount feeDiscount;
    private CompletedRace completedRace;
}
