package mk.ukim.finki.wp.running.service.impl;

import lombok.RequiredArgsConstructor;
import mk.ukim.finki.wp.running.model.domain.User;
import mk.ukim.finki.wp.running.model.enumerations.Medal;
import mk.ukim.finki.wp.running.model.exceptions.InvalidUsernameException;
import mk.ukim.finki.wp.running.repository.CompletedRaceRepository;
import mk.ukim.finki.wp.running.repository.UserRepository;
import mk.ukim.finki.wp.running.service.RunnerService;
import org.springframework.stereotype.Service;

import java.util.EnumMap;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class RunnerServiceImpl implements RunnerService {
    private final UserRepository userRepository;
    private final CompletedRaceRepository completedRaceRepository;

    @Override
    public User findById(String userId) {
        return userRepository.findById(userId).orElseThrow(InvalidUsernameException::new);
    }

    @Override
    public Map<Medal, Long> countMedalsByUserId(String userId) {
        Map<Medal, Long> medalCountMap = new EnumMap<>(Medal.class);

        // Initialize all medal types
        for (Medal medal : Medal.values()) {
            medalCountMap.put(medal, 0L);
        }

        completedRaceRepository
                .countMedalsByUserId(userId)
                .forEach(medalCount -> {
                    if (medalCount.getMedal() != null) {
                        medalCountMap.put(medalCount.getMedal(), medalCount.getCount());
                    }
                });

        return medalCountMap;
    }
}
