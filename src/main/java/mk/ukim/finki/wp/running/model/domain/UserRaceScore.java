package mk.ukim.finki.wp.running.model.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Entity
@Data
@NoArgsConstructor
public class UserRaceScore {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne(fetch = FetchType.LAZY)
    private User user;
    @OneToOne(fetch = FetchType.LAZY)
    @JsonManagedReference
    Registration registration;
    private double points;
    private double penalty;
    private double score;
    private LocalDateTime created;

    public UserRaceScore(User user, Registration registration, double points, double penalty, double score, LocalDateTime created) {
        this.user = user;
        this.registration = registration;
        this.points = points;
        this.penalty = penalty;
        this.score = score;
        this.created = created;
    }
}
