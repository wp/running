package mk.ukim.finki.wp.running.model.DTO;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class EventDto {
    private String name;
    private String description;
    private LocalDateTime time;
    private String location;
    private List<Long> subEventIds;

    public EventDto(String name, String description, LocalDateTime time, String location, List<Long> subEventIds) {
        this.name = name;
        this.description = description;
        this.time = time;
        this.location = location;
        this.subEventIds = subEventIds;
    }
}

