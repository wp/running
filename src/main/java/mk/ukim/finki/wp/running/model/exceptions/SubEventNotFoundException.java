package mk.ukim.finki.wp.running.model.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class SubEventNotFoundException extends RuntimeException{
    public SubEventNotFoundException(Long id) {
        super(String.format("The sub event with the id: %d doesn't exist", id));
    }
}
