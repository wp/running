package mk.ukim.finki.wp.running.repository;

import mk.ukim.finki.wp.running.model.domain.Voucher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VoucherRepository extends JpaRepository<Voucher, Long> {
}
