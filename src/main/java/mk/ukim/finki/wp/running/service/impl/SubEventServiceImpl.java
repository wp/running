package mk.ukim.finki.wp.running.service.impl;

import lombok.RequiredArgsConstructor;
import mk.ukim.finki.wp.running.model.domain.Event;
import mk.ukim.finki.wp.running.model.domain.SubEvent;
import mk.ukim.finki.wp.running.model.enumerations.SubEventLength;
import mk.ukim.finki.wp.running.model.exceptions.InvalidEventIdException;
import mk.ukim.finki.wp.running.model.exceptions.InvalidSubEventIdException;
import mk.ukim.finki.wp.running.repository.EventRepository;
import mk.ukim.finki.wp.running.repository.SubEventRepository;
import mk.ukim.finki.wp.running.service.SubEventService;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
@RequiredArgsConstructor
public class SubEventServiceImpl implements SubEventService {
    private final SubEventRepository subEventRepository;
    private final EventRepository eventRepository;


    @Override
    public List<SubEvent> findAll() {
        return subEventRepository.findAll();
    }

    @Override
    public List<SubEvent> findAllByEventId(Long eventId) {
        return subEventRepository.findAllByEventId(eventId);
    }

    @Override
    public List<SubEvent> searchByName(String name) {
        return subEventRepository.findSubEventsByNameContaining(name);
    }

    @Override
    public SubEvent findById(Long id) {
        return subEventRepository.findById(id).orElseThrow(() -> new InvalidSubEventIdException(id));
    }

    @Override
    public SubEvent save(String name, SubEventLength length, String imageUrl, String startInfo, Long eventId, LocalDateTime startTime) {
        Event event = eventRepository.findById(eventId).orElseThrow(() -> new InvalidEventIdException(eventId));
        SubEvent subEvent = new SubEvent();
        subEvent.setName(name);
        subEvent.setLength(length);
        subEvent.setImageUrl(imageUrl);
        subEvent.setStartInfo(startInfo);
        subEvent.setEvent(event);
        subEvent.setStartTime(startTime);
        return subEventRepository.save(subEvent);
    }

    @Override
    public SubEvent edit(Long id, String name, SubEventLength length, String imageUrl, String startInfo, Long eventId, LocalDateTime startTime) {
        SubEvent subEvent = this.subEventRepository.findById(id).orElseThrow();
        subEvent.setName(name);
        subEvent.setLength(length);
        subEvent.setImageUrl(imageUrl);
        subEvent.setStartInfo(startInfo);
        subEvent.setStartTime(startTime);
        subEvent.setEvent(this.eventRepository.findById(eventId).orElseThrow());
        this.subEventRepository.save(subEvent);
        return subEvent;

    }

    @Override
    public void delete(Long id) {
        subEventRepository.deleteById(id);
    }

    @Override
    public List<SubEvent> findAllUpcomingByEvent(Event event) {
        return subEventRepository.findSubEventsByEventAndStartTimeAfter(event, LocalDateTime.now());
    }

    @Override
    public String convertLength(SubEventLength length) {
        return switch (length) {
            case FIVE -> "5km";
            case TEN -> "10km";
            case HALF_MARATHON -> "Полу-Маратон (21km)";
            case FULL_MARATHON -> "Маратон (42km)";
            case FULL_MARATHON_RELAY -> "Маратон - Штафета (42km)";
        };
    }
}
