package mk.ukim.finki.wp.running.service.impl;

import lombok.RequiredArgsConstructor;
import mk.ukim.finki.wp.running.model.domain.Request;
import mk.ukim.finki.wp.running.model.enumerations.RequestStatus;
import mk.ukim.finki.wp.running.model.domain.SubEvent;
import mk.ukim.finki.wp.running.model.domain.User;
import mk.ukim.finki.wp.running.model.exceptions.InvalidRequestIdException;
import mk.ukim.finki.wp.running.model.exceptions.RequestByUserForSubEventAlreadyExistsException;
import mk.ukim.finki.wp.running.repository.RequestRepository;
import mk.ukim.finki.wp.running.repository.UserRepository;
import mk.ukim.finki.wp.running.service.RegistrationService;
import mk.ukim.finki.wp.running.service.RequestService;
import mk.ukim.finki.wp.running.service.SubEventService;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.time.LocalDate;
import java.util.List;

@Service
@RequiredArgsConstructor
public class RequestServiceImpl implements RequestService {

    private final RequestRepository requestRepository;
    private final UserRepository userRepository;
    private final SubEventService subEventService;
    private final RegistrationService registrationService;

    @Override
    public List<Request> findAll() {
        return requestRepository.findAll();
    }

    @Override
    public List<Request> findAllByStatus(RequestStatus status) {
        return requestRepository.findAllByStatus(status);
    }

    @Override
    public List<Request> findAllByUserPrincipal(Principal principal) {
        User user = userRepository.getReferenceById(principal.getName());
        return requestRepository.findAllByUserOrderByTimestampDesc(user);
    }


    @Override
    public Request findById(Long id) {
        return requestRepository.findById(id).orElseThrow(InvalidRequestIdException::new);
    }

    @Override
    public Request create(Principal principal, Long subEventId, String message) {
        if (!requestRepository.findAllByUser_IdAndSubEvent_Id(principal.getName(), subEventId).isEmpty()) {
            throw new RequestByUserForSubEventAlreadyExistsException();
        }

        User user = userRepository.getReferenceById(principal.getName());
        SubEvent subEvent = subEventService.findById(subEventId);
        return requestRepository.save(new Request(user, subEvent, message));
    }

    @Override
    public Request edit(Long id, Principal principal, Long subEventId, String message) {
        Request request = findById(id);

        request.setUser(userRepository.getReferenceById(principal.getName()));
        request.setSubEvent(subEventService.findById(subEventId));
        request.setMessage(message);

        return requestRepository.save(request);
    }


    @Override
    public Request delete(Long id) {
        Request request = findById(id);
        requestRepository.deleteById(id);
        return request;
    }

    @Override
    public Request changeStatus(Long id, String newStatus) {
        Request request = findById(id);
        var status = RequestStatus.valueOf(newStatus);
        request.setStatus(status);

        if (status.equals(RequestStatus.APPROVED)) {
            registrationService.save(
                    request.getUser(),
                    request.getSubEvent(),
                    LocalDate.from(request.getTimestamp())
            );
        } else if (status.equals(RequestStatus.REJECTED)) {
            registrationService.deleteByUserIdAndSubEventId(
                    request.getUser().getId(),
                    request.getSubEvent().getId()
            );
        }

        requestRepository.save(request);
        return request;
    }
}
