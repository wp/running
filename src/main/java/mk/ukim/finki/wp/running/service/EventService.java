package mk.ukim.finki.wp.running.service;


import mk.ukim.finki.wp.running.model.domain.Event;
import mk.ukim.finki.wp.running.model.domain.SubEvent;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

public interface EventService {
    List<Event> findAll();
    List<Event> findAllByName(String name);
    List<Event> getFutureEvents();
    List<Event> getPastEvents();
    List<Event> searchByName(String name);
    Event findById(Long id);
    Event findByName(String name);
    void save(String name, String description, LocalDateTime time, String location);
    void edit(Long id, String name, String description, LocalDateTime time, String location);
    void deleteById(Long id);
    void addSubEventToEvent(Long eventId, Long subEventId);
    void removeSubEventFromEvent(Long eventId, Long subEventId);
    Map<Event, List<SubEvent>> findAllUpcoming();
}

