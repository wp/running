package mk.ukim.finki.wp.running.model.enumerations;

public enum SubEventLength {
    FIVE,
    TEN,
    HALF_MARATHON,
    FULL_MARATHON,
    FULL_MARATHON_RELAY,


}
